from genericpath import isfile
import os
import socket
import parser

server_port = 12000
server_host = "localhost"
backlog = 10


def fileExists(filepath):
  if os.path.isfile(filepath):
    return True
  else:
     return False


server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((server_host, server_port))
server.listen(backlog)
print(f"Server listening on port {server_port}")

while True:
  client_conn, client_addr = server.accept()

  request = client_conn.recv(1024).decode()
  HTTPobj = parser.factory(request)

  print(f"headers: {HTTPobj.headers}\npath: {HTTPobj.http_path}\nverb: {HTTPobj.http_verb}") 

  if(HTTPobj.http_path == '/' or HTTPobj.http_path == '/index.html'):
    path = "../templates/index.html"
    file = open(os.path.join(os.path.dirname(__file__), path), 'r')
    content = file.read()
    file.close
    res = f"HTTP/1.1 200 OK\n\n{content}"
    client_conn.sendall(res.encode())
  
  elif(HTTPobj.http_path != ""):
    path = f"../templates{HTTPobj.http_path}"
    if(fileExists(os.path.join(os.path.dirname(__file__), path))):
        file = open(os.path.join(os.path.dirname(__file__), path), 'r')
        content = file.read()
        file.close
        res = f"HTTP/1.1 200 OK\n\n{content}"
        client_conn.sendall(res.encode())
    else:
      client_conn.sendall('HTTP/1.1 400 NOT FOUNT\n\n<h1>NOT FOUNT</h1>'.encode())

  else:
      client_conn.sendall('HTTP/1.1 400 NOT FOUNT\n\n<h1>NOT FOUNT</h1>'.encode())

    


  # res = 'HTTP/1.1 200 OK\n\n<h1>Hello world</h1><strong></strong>'
  # client_conn.sendall(res.encode())

  client_conn.close()