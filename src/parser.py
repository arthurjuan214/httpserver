class HTTPObject:
  def __init__(self, http_verb, http_path, headers, body = ''):
    self.http_verb = http_verb,
    self.http_path = http_path
    self.headers = headers
    self.body = body



def factory(request):
  # print(request)
  headers = request.split('\n')
  http_path = headers[0].split()[1]
  # print(http_path)
  http_verb = headers[0].split()[0]
  # print(headers)
  # print(http_verb)

  return HTTPObject(http_verb, http_path, headers)
